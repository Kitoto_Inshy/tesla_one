function factorial(x) {
    if (x === 1) {
        return x;
    } else {
        return x * factorial(x - 1);
    }
}

let num = "Some number";

do {
    num = prompt("Enter number", num);
} while (!Number.isInteger(+num));

alert(factorial(num));