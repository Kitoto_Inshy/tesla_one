$('li.tabs-title').click(function () {
    $(this).addClass('active')
    .siblings()
    .removeClass('active')
    .closest('div.centered-content')
    .find('li.tabs-text')
    .removeClass('active')
    .eq($(this).index())
    .addClass('active');
});