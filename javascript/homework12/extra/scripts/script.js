const imgs = document.querySelectorAll('.image-to-show');
const divWrp = document.querySelector('.images-wrapper');

const stopBtn = createButton('Прекратить', 'block');
const continueBtn = createButton('Возобновить показ', 'block');
const timer = document.createElement('div');
timer.innerHTML = `${(timeToNextImg())/1000}`;
divWrp.before(stopBtn);
divWrp.before(timer);
divWrp.after(continueBtn);
stopBtn.addEventListener('click', onClickStopShow);
continueBtn.addEventListener('click', onClickContShow);

let timeInterval = setInterval(showImgs, 10000);

let i = 1;
function showImgs() {
    if (i === imgs.length) {
        i = 1;
        imgs[0].classList.add('actived')
        imgs[imgs.length - 1].classList.remove('actived')
    } else {
        imgs[i].classList.add('actived')
        imgs[i - 1].classList.remove('actived')
        i++;
    };
};

function createButton(text, display) {
    const btn = document.createElement('button');
    btn.innerHTML = text;
    btn.style.display = display;
    return btn;
};

function onClickStopShow(event) {
    event.preventDefault();
    clearInterval(timeInterval);
};

function onClickContShow(event) {
    event.preventDefault();
    if (timeInterval) {
        clearInterval(timeInterval);
    };
    timeInterval = setInterval(showImgs, 10000);
};

function timeToNextImg() {
    let startPoint = new Date;
    let finalPoint = 10000;
    let difference = (finalPoint - (new Date - startPoint));
    return difference;
};





















// let timer = document.createElement('div');
// let minutes = document.createElement('span');
// let seconds = document.createElement('span');
// let milliseconds = document.createElement('span');
// minutes.innerText = '0';
// seconds.innerText = '10';
// milliseconds.innerText = '0';
// minutes.style.margin = '10px';
// seconds.style.margin = '10px';
// milliseconds.style.margin = '10px';
// timer.append(minutes);
// timer.append(seconds);
// timer.append(milliseconds);
// document.body.append(timer);
// const startDate = new Date;
// function timerCountMilliseconds() {
//     const finalDate = 10000;
//     let timeInMilliseconds = (finalDate - (new Date - startDate));
//     if (timeInMilliseconds < 0) {
//         clearInterval(interval);
//         timeInMilliseconds = 0;
//         timer.remove();
//         document.write('')
//     }
//     minutes.innerText = Math.trunc(timeInMilliseconds / 60000).toString();
//     timeInMilliseconds = timeInMilliseconds % 60000;
//     seconds.innerText = Math.trunc(timeInMilliseconds / 1000).toString();
//     timeInMilliseconds = timeInMilliseconds % 1000;
//     milliseconds.innerText = timeInMilliseconds.toString();
// }
// const interval = setInterval(timerCountMilliseconds, 1);