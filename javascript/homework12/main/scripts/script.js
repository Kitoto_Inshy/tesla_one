const imgs = document.querySelectorAll('.image-to-show');
const divWrp = document.querySelector('.images-wrapper');

const stopBtn = createButton('Прекратить', 'block');
const continueBtn = createButton('Возобновить показ', 'block');
divWrp.before(stopBtn);
divWrp.after(continueBtn);
stopBtn.addEventListener('click', onClickStopShow);
continueBtn.addEventListener('click', onClickContShow);

let timeInterval = setInterval(showImgs, 10000);

let i = 1;
function showImgs() {
    if (i === imgs.length) {
        i = 1;
        imgs[0].classList.add('actived')
        imgs[imgs.length - 1].classList.remove('actived')
    } else {
        imgs[i].classList.add('actived')
        imgs[i - 1].classList.remove('actived')
        i++;
    };
};

function createButton(text, display) {
    const btn = document.createElement('button');
    btn.innerHTML = text;
    btn.style.display = display;
    return btn;
};

function onClickStopShow(event) {
    event.preventDefault();
    clearInterval(timeInterval);
};

function onClickContShow(event) {
    event.preventDefault();
    if (timeInterval) {
        clearInterval(timeInterval);
    };
    timeInterval = setInterval(showImgs, 10000);
};