const input = document.querySelector('.price-input');
input.style.display = 'block';
input.style.marginTop = '30px';
const priceSpan = document.createElement('span');
const btnX = document.createElement('button');
btnX.innerText = 'X';
const warningMasenge = document.createElement('p');
warningMasenge.innerText = 'Please enter correct price';

input.addEventListener('focus', onFocus);
input.addEventListener('blur', onBlur);
btnX.addEventListener('click', onClick);

function onFocus(event) {
  event.target.style.outline = '1px solid yellowgreen';
  input.style.color = 'black';
  warningMasenge.remove();
  priceSpan.remove();
  btnX.remove();
};

function onBlur(event) {
  event.target.style.outline = '';
  priceSpan.innerText = `Текущая цена: ${input.value}`;
  if (input.value > 0) {
    input.style.color = 'green';
    input.before(priceSpan);
    priceSpan.after(btnX);
  } else {
    event.target.style.outline = '1px solid red';
    input.after(warningMasenge);
    input.style.color = 'black';
  };
};

function onClick(event) {
  event.preventDefault();
  priceSpan.remove();
  btnX.remove();
  input.value = '';
};