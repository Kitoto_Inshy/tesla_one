document.addEventListener('DOMContentLoaded', (() => {
    if (localStorage.getItem('theme') === 'dark') {
        setTheme()
    } else {
        localStorage.setItem('theme', 'standart')
    };
}));

const sideBar = document.querySelector('.area-sidebar');
const themeChangeBtn = document.createElement('a');
themeChangeBtn.innerHTML = 'Change Theme';
themeChangeBtn.classList.add('sidebar-item');
themeChangeBtn.setAttribute('href', '');
sideBar.append(themeChangeBtn);

themeChangeBtn.addEventListener('click', (() => {
    event.preventDefault();
    if (localStorage.getItem('theme') === 'standart') {
        localStorage.setItem('theme', 'dark');
        setTheme()
    } else if (localStorage.getItem('theme') === 'dark') {
        localStorage.setItem('theme', 'standart');
        setTheme();
    };
}));

function setTheme() {
    document.querySelectorAll('.standart').forEach ((elem) => {
        elem.classList.toggle('dark');
    });
};