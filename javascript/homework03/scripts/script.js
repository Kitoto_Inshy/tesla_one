function Mathfuc(x, y, z) {
    switch (z) {
        case "*":
            return x * y;
        case "/":
            return x / y;
        case "+":
            return +x + +y;
        case "-":
            return x - y;
    }
}

let num1 = "Some number";
let num2 = "Some number";

while (!Number.isInteger(+num1) || !Number.isInteger(+num2)) {
    num1 = prompt("Enter number 1", num1);
    num2 = prompt("Enter number 2", num2);
}

let oper = prompt("Enter sign");

alert(Mathfuc(num1, num2, oper));