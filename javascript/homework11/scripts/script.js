const divWrapp = document.querySelector('body');
const btns = document.querySelectorAll('.btn');

divWrapp.addEventListener('keydown', onKeyDown);

function onKeyDown(e) {
    btns.forEach(elem => {
        if ((e.key).toLocaleUpperCase() === (elem.innerHTML).toLocaleUpperCase()) {
            elem.classList.add('actived');
        } else {
            elem.classList.remove('actived');
        }
    });
};