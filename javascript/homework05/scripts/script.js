function createNewUser() {

    let user = {

        first_name : prompt("Enter your name"),
        last_name : prompt("Enter your surname"),
        birthday: prompt("Enter your birthday like dd.mm.yyyy"),

        getLogin() {
            return (this.first_name[0] + this.last_name).toLocaleLowerCase();
        },

        getPassword() {
            return this.first_name[0] + (this.last_name).toLocaleLowerCase() + this.birthday.substr(-4, 4);
        },

        getAge() {
            let s = this.birthday.split(".");
            return (new Date()).getFullYear() - (new Date(s[2], s[1], s[0])).getFullYear();
        },
    };

    return user;
}

let uset = createNewUser();
console.log(uset.getLogin());
console.log(uset.getAge());
console.log(uset.getPassword());
console.log(uset);