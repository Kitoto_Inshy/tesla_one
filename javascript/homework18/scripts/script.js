let student = {
    name: prompt("Enter your name"),
    lastName: prompt("Enter your surname"),
    tabel: {},
}

let subject;
do {
    subject = prompt("Enter subject");

    if (subject) student.tabel[subject] = prompt("Enter mark");

} while (subject)

let marks = 0;
let badMarks = 0;
let middleMark = 0;

for (let key in student.tabel) {
    marks++;

    if (student.tabel[key] <= 4) badMarks++;
    
    middleMark += +student.tabel[key];
}

if (badMarks === 0) alert("Студент переведен на следующий курс");

if ((middleMark / marks) >= 7) alert("Студенту назначена стипендия");

console.log(student);