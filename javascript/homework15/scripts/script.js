$('a[data-name]').click(function (event) {
    event.preventDefault();
    let $id = $(`section[data-name="${event.currentTarget.dataset["name"]}"]`);
    $('html').animate({ scrollTop: $id.offset().top }, 500);
    console.log($id.offset().top);
});


$(window).scroll(onScroll);

function onScroll(e) {
    const className = 'btn-scroll-top';
    if (window.scrollY > window.innerHeight) {
        if (!$(`.${className}`).length) {
            $('<button> &#8896; </button>')
                .css({
                    position: 'fixed',
                    right: '20px',
                    bottom: '30px',
                    fontSize: '25px',
                    borderRadius: '25%',
                    border: '0px',
                    background: '#14b9d5',
                    color: '#fff',
                })
                .addClass(className)
                .click(scrollTop)
                .appendTo(document.body);
        }
    } else {
        $(`.${className}`).remove();
    }
}

function scrollTop() {
    $("html, body").animate({ scrollTop: 0 }, 1000);
}


$('.show-btn').click(function (event) {
    event.preventDefault();
    $(this).next().slideToggle(800);
    if ($(this).text() === 'Hide section') {
        $(this).text('Show section');
    } else {
        $(this).text('Hide section');
    };
});