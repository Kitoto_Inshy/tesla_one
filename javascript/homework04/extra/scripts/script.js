function createNewUser() {

    let user = {

        getLogin() {
            let login;
            let loginThis;

            if (this.first_name && this.last_name) {
                loginThis = (this.first_name[0] + this.last_name).toLocaleLowerCase();
            } else {
                login = (first_name[0] + last_name).toLocaleLowerCase();
            }
            
            return login || loginThis;
        },

        setFirstName(newFirstName) {
            Object.defineProperty(this, "first_name", {
                writable : true,
            });
            this.first_name = newFirstName;
            Object.defineProperty(this, "first_name", {
                writable: false,
            });
        },

        getFirstName() {
            return this.first_name || first_name;
        },

        setLastName(newLastName) {
            Object.defineProperty(this, "last_name", {
                writable : true,
            });
            this.last_name = newLastName;
            Object.defineProperty(this, "last_name", {
                writable: false,
            });
        },

        getLastName() {
            return this.last_name || last_name;;
        },
    };

    Object.defineProperty(this, "first_name", {
        value: prompt("Enter your name"),
        writable: false,
    });

    Object.defineProperty(this, "last_name", {
        value: prompt("Enter your surname"),
        writable: false,
    });

    return user;
}

let uset = createNewUser();
console.log(uset.getLogin());

uset.setFirstName("Test");
uset.setLastName("To test");

console.log(uset.getFirstName());
console.log(uset.getLastName());
console.log(uset.getLogin());