const form = document.querySelector('form');
const inputs = document.querySelectorAll('input');
const btnSbm = document.querySelector('button');
const warnMess = createWarning();

form.addEventListener('mousedown', onMouseDown);
form.addEventListener('mouseup', onMouseUp);
btnSbm.addEventListener('click', onClick);
inputs.forEach(elem => elem.addEventListener('input', () => {
    if (warnMess !== undefined) {
        warnMess.remove();
    };
}));

function onMouseDown(event) {
    event.stopPropagation();
    if (event.target.tagName === 'I') {
        event.target.classList.remove('fa-eye');
        event.target.classList.add('fa-eye-slash');
        event.target.previousElementSibling.type = 'text';
    };
};

function onMouseUp(event) {
    event.stopPropagation();
    if (event.target.tagName === 'I') {
        event.target.classList.remove('fa-eye-slash');
        event.target.classList.add('fa-eye');
        event.target.previousElementSibling.type = 'password';
    };
};

function onClick(event) {
    event.preventDefault();
    if (inputs[0].value === inputs[1].value) {
        alert('You are welcome');
    } else {
        inputs[1].nextElementSibling.after(warnMess);
    };
};

function createWarning() {
    const warn = document.createElement('p');
    warn.innerHTML = 'Нужно ввести одинаковые значения';
    warn.style.color = 'red';
    return warn;
};