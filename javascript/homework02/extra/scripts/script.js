"use strict";

let m;

while (!Number.isInteger(m)) {
    m = +prompt("Enter number 1");
    if (!Number.isInteger(m)) {
        alert("Error");
    }
}

let n;

do {
    n = prompt("Enter number 2");
} while ((n < m) && !Number.isInteger(n));
if (!Number.isInteger(+n)) {
    alert("Error");
}


nextNumber:
for (let i = m; i <= n; i++) {
    for (let j = 2; j <= n; j++) {
        if (i % j === 0) {
            continue nextNumber;
        } else {
            console.log(i);
        }
    }
}