function generateList(arr) {
    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] === 'object' && !Array.isArray(arr[i])) {
            arr[i] = Object.entries(arr[i]);
        };
    }
    return `<ul>${arr.map(elem => `<li>${Array.isArray(elem) ? generateList(elem) : elem}</li>`).join('')}</ul>`;
};

function createElem(elem) {
    const innerElem = document.createElement('div');
    innerElem.innerHTML = elem;
    return innerElem;
};


let array = ['hello', { '1': '2', '3': 'sea', 'user': 23, mem:{Car: "BMW", Model: '100'}, },
    'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv',
    { '1': '2', '3': 'sea', 'user': 23, }, 'Rivne'];

let s = generateList(array);
let divGen = createElem(s);
let divTimer = createElem();
divTimer.textContent = '10';
window.addEventListener('load', () => {
    document.body.prepend(divGen);
    document.body.prepend(divTimer);
});

let ti = 10;
let t = 1;
let timeId = setInterval(() => {
    ti = ti - t;
    divTimer.textContent = ti;
    if (ti === -1) {
        clearInterval(timeId);
        divGen.remove();
        divTimer.remove();
    };
}, 1000);