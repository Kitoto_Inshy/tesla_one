function generateList(arr) {
    return `<ul>${arr.map(elem => `<li>${elem}</li>`).join('')}</ul>`;
}

function createElem(elem) {
    const innerElem = document.createElement('div');
    innerElem.innerHTML = elem;
    return innerElem;
}

function addElem(inputData) {
    let s = generateList(inputData);
    let divGen = createElem(s);
    window.addEventListener('load', () => {
        document.body.prepend(divGen);
    });
}

let array = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

addElem(array);